<?php
class Htmalpage{
    protected $title = "Hello";
    protected $body = "world";
    
    function __construct($title = "", $body = ""){
        if($title != "" and $body != ""){
        $this->title = $title;
        $this->body = $body;
    }
}
    public function view(){
        echo "<html>
        <head>
        <title>$this->title</title>
        </head>
        <body>$this->body</body>
        </html>";
    }
}
  
class coloredHtmalpage extends Htmalpage{
    protected $color = 'black'; //הגדרת צבע דיפולטיבי
    public function __set($property,$value){
        if($property == 'color'){
            $colors = array('orange', 'pink', 'purple', 'blue'); //הגדרת מערך ובו הצבעים שמותר להכניס
            if(in_array($value, $colors)){
                $this->color = $value; //אם הצבע נמצא במערך שהוגדר תודפס הודעה בצבע שהוכנס
            }
            elseif(in_array($value, $colors)==FALSE){  
                echo '<script>alert("Invalid Color!");</script>';//אם לא, תופיע הודעת שגיאה וההודעה תודפס בצבע הדיפולטיבי שהוגדר
            }
        }
    }
    public function view(){
        echo "<p style = 'color:$this->color'>$this->body</p>";
    }
}
#comment 1
?>



